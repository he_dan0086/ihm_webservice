'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('MainCtrl', ['$scope', '$http', function ($scope,$http) {
    $scope.author = 'Dan HE';
    $http.get('../../assets/main.json')
        .success(function (data) {
            $scope.main = data;
        })
        .error(function (error){
            console.log(error);
        });
  }]);
